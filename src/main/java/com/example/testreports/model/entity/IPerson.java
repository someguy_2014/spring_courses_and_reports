package com.example.testreports.model.entity;

public interface IPerson {

    Long getId();
    EmbeddablePerson getPerson();
    void setPerson(EmbeddablePerson embeddablePerson);
}
