package com.example.testreports.model.entity;

import com.example.testreports.model.enums.CourseType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "courses")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Course extends Auditable {

    @Column(name = "name", nullable = false)
    private String name;

    @Enumerated(EnumType.STRING)
    private CourseType type;

    @ManyToOne
    @JoinColumn(name = "teacher_id", nullable = false)
    private Teacher teacher;

    @OneToMany(mappedBy = "course")
    private Set<Group> groups = new HashSet<>();
}
