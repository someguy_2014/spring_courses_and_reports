package com.example.testreports.model.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "teachers")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Teacher extends Auditable implements IPerson {

    @Embedded
    private EmbeddablePerson person;

    @OneToMany(mappedBy="teacher")
    private Set<Course> courses = new HashSet<>();
}
