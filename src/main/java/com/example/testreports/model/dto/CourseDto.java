package com.example.testreports.model.dto;

import com.example.testreports.model.entity.Course;
import com.example.testreports.model.enums.CourseType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CourseDto {

    private Long id;
    private String name;
    @Enumerated(EnumType.STRING)
    private CourseType type;

    public Course toCourse() {
        Course course = new Course();

        course.setName(getName());
        course.setType(getType());

        return course;
    }

    public static CourseDto from(Course course) {
        CourseDto courseDto = new CourseDto();

        courseDto.setId(course.getId());
        courseDto.setName(course.getName());
        courseDto.setType(course.getType());

        return courseDto;
    }

}
