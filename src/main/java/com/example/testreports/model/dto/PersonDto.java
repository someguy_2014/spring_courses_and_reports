package com.example.testreports.model.dto;

import com.example.testreports.model.entity.EmbeddablePerson;
import com.example.testreports.model.entity.IPerson;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.lang.reflect.InvocationTargetException;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PersonDto {

    private Long id;
    private String name;
    private Integer age;

    public static PersonDto from(IPerson person) {
        PersonDto personDto = new PersonDto();

        personDto.setId(person.getId());
        personDto.setName(person.getPerson().getName());
        personDto.setAge(person.getPerson().getAge());

        return personDto;
    }

    public <T> T toPerson(Class<T> clazz) {
        try {
            var person = clazz.getConstructor().newInstance();

            if(!IPerson.class.isInstance(person)) throw new IllegalAccessException();

            ((IPerson)person).setPerson(new EmbeddablePerson(getName(), getAge()));

            return person;
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            throw new RuntimeException(e);
        }
    }

}
