package com.example.testreports.model.dto;

import com.example.testreports.model.entity.Group;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

import static java.util.stream.Collectors.toSet;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GroupDto {

    private Long id;
    private String name;
    private Set<PersonDto> students;

    public Group toGroup() {
        Group group = new Group();

        group.setName(getName());

        return group;
    }

    public static GroupDto from(Group group) {
        GroupDto groupDto = new GroupDto();

        groupDto.setId(group.getId());
        groupDto.setName(group.getName());
        groupDto.setStudents(group.getStudents().stream().map(student -> PersonDto.from(student)).collect(toSet()));

        return groupDto;
    }

}
