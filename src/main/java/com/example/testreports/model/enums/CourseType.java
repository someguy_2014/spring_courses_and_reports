package com.example.testreports.model.enums;

public enum CourseType {
    main, secondary;
}
