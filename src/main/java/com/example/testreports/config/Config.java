package com.example.testreports.config;

import com.example.testreports.repository.CourseRepository;
import com.example.testreports.repository.GroupRepository;
import com.example.testreports.repository.StudentRepository;
import com.example.testreports.repository.TeacherRepository;
import com.example.testreports.service.CourseService;
import com.example.testreports.service.GroupService;
import com.example.testreports.service.RegisterService;
import com.example.testreports.service.ReportingService;
import com.example.testreports.service.StudentService;
import com.example.testreports.service.TeacherService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Config {

    @Bean
    public TeacherService teacherService(TeacherRepository teacherRepository){
        return new TeacherService(teacherRepository);
    }

    @Bean
    public StudentService studentService(StudentRepository studentRepository){
        return new StudentService(studentRepository);
    }

    @Bean
    public CourseService courseService(CourseRepository courseRepository){
        return new CourseService(courseRepository);
    }

    @Bean
    public GroupService groupService(GroupRepository groupRepository){
        return new GroupService(groupRepository);
    }

    @Bean
    public RegisterService registerService(StudentService studentService,
                                           TeacherService teacherService,
                                           CourseService courseService,
                                           GroupService groupService){
        return new RegisterService(studentService, teacherService, courseService, groupService);
    }

    @Bean
    public ReportingService reportingService(StudentRepository studentRepository,
                                             TeacherRepository teacherRepository,
                                             CourseRepository courseRepository){
        return new ReportingService(studentRepository, teacherRepository, courseRepository);
    }
}
