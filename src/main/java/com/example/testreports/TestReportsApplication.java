package com.example.testreports;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class TestReportsApplication {

    public static void main(String[] args) {
        SpringApplication.run(TestReportsApplication.class, args);
    }

}
