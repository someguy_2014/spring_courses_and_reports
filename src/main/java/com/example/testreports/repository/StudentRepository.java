package com.example.testreports.repository;

import com.example.testreports.model.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface StudentRepository extends JpaRepository<Student, Long> {

    @Query(value = "SELECT students.* FROM students "
            + " INNER JOIN students_groups ON students_groups.student_id = students.id "
            + " INNER JOIN groups ON students_groups.group_id = groups.id "
            + " INNER JOIN courses ON courses.id = groups.course_id "
            + " WHERE courses.id = :courseId ", nativeQuery = true)
    List<Student> getStudentsByCourse(Long courseId);

    @Query(value = "SELECT students.* FROM students "
            + " INNER JOIN students_groups ON students_groups.student_id = students.id "
            + " INNER JOIN groups ON students_groups.group_id = groups.id "
            + " WHERE groups.id = :groupId ", nativeQuery = true)
    List<Student> getStudentsByGroup(Long groupId);

    @Query(value = "SELECT students.* FROM students "
            + " INNER JOIN students_groups ON students_groups.student_id = students.id "
            + " LEFT JOIN groups ON students_groups.group_id = groups.id "
            + " LEFT JOIN courses ON courses.id = groups.course_id "
            + " WHERE courses.id = :courseId AND groups.id = :groupId ", nativeQuery = true)
    List<Student> getStudentsByCourseAndGroup(Long courseId, Long groupId);

    @Query(value = "SELECT students.* FROM students "
            + " INNER JOIN students_groups ON students_groups.student_id = students.id "
            + " INNER JOIN groups ON students_groups.group_id = groups.id "
            + " INNER JOIN courses ON courses.id = groups.course_id "
            + " WHERE courses.id = :courseId AND students.age > :age ", nativeQuery = true)
    List<Student> getStudentsByCourseOlderThan(Long courseId, int age);
}
