package com.example.testreports.repository;

import com.example.testreports.model.entity.Group;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface GroupRepository extends JpaRepository<Group, Long> {

    List<Group> findByCourseId(Long courseId);

    Optional<Group> findByIdAndCourseId(Long id, Long courseId);
}
