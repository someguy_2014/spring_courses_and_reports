package com.example.testreports.repository;

import com.example.testreports.model.entity.Course;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface CourseRepository extends JpaRepository<Course, Long> {

    List<Course> findByTeacherId(Long teacherId);

    Optional<Course> findByIdAndTeacherId(Long id, Long teacherId);
}
