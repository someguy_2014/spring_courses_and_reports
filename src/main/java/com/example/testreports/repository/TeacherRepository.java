package com.example.testreports.repository;

import com.example.testreports.model.entity.Student;
import com.example.testreports.model.entity.Teacher;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface TeacherRepository extends JpaRepository<Teacher, Long> {
    @Query(value = "SELECT teachers.* FROM teachers "
            + " INNER JOIN courses ON courses.teacher_id = teachers.id "
            + " INNER JOIN groups ON courses.id = groups.course_id "
            + " WHERE courses.id = :courseId AND groups.id = :groupId ", nativeQuery = true)
    List<Teacher> getTeachersByCourseAndGroup(Long courseId, Long groupId);
}
