package com.example.testreports.controller;

import com.example.testreports.model.dto.GroupDto;
import com.example.testreports.model.entity.Course;
import com.example.testreports.model.entity.Group;
import com.example.testreports.service.CourseService;
import com.example.testreports.service.GroupService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static java.util.stream.Collectors.toList;

@RestController
@RequestMapping("/course/{courseId}")
public class GroupController {

    private final CourseService courseService;
    private final GroupService groupService;

    public GroupController(CourseService courseService, GroupService groupService) {
        this.courseService = courseService;
        this.groupService = groupService;
    }

    @PostMapping
    public ResponseEntity<GroupDto> add(@PathVariable Long courseId,
                                        @RequestBody GroupDto groupDto) {
        Course course = courseService.get(courseId);
        Group group = groupService.add(course, groupDto.toGroup());

        return new ResponseEntity<>(GroupDto.from(group), HttpStatus.CREATED);
    }

    @GetMapping
    public ResponseEntity<List<GroupDto>> list(@PathVariable Long courseId) {
        List<GroupDto> groupDto = groupService.list(courseId).stream().map(GroupDto::from).collect(toList());

        return new ResponseEntity<>(groupDto, HttpStatus.OK);
    }

    @GetMapping("{id}")
    public ResponseEntity<GroupDto> get(@PathVariable Long courseId,
                                        @PathVariable Long id) {
        Group group = groupService.get(courseId, id);

        return new ResponseEntity<>(GroupDto.from(group), HttpStatus.OK);
    }

    @PutMapping("{id}")
    public ResponseEntity<GroupDto> update(@PathVariable Long courseId,
                                           @PathVariable Long id,
                                           @RequestBody GroupDto groupDto) {
        Group group = groupService.update(courseId, id, groupDto.toGroup());

        return new ResponseEntity<>(GroupDto.from(group), HttpStatus.OK);
    }

    @DeleteMapping("{id}")
    public ResponseEntity<GroupDto> delete(@PathVariable Long courseId,
                                           @PathVariable Long id) {
        Group group = groupService.delete(courseId, id);

        return new ResponseEntity<>(GroupDto.from(group), HttpStatus.OK);
    }

}
