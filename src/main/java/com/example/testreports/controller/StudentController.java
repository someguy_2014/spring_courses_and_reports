package com.example.testreports.controller;

import com.example.testreports.model.dto.PersonDto;
import com.example.testreports.model.entity.Student;
import com.example.testreports.service.StudentService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static java.util.stream.Collectors.toList;

@RestController
@RequestMapping("/student")
public class StudentController {

    private final StudentService studentService;

    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }

    @PostMapping
    public ResponseEntity<PersonDto> add(@RequestBody PersonDto personDto) {
        Student student = studentService.add(personDto.toPerson(Student.class));

        return new ResponseEntity<>(PersonDto.from(student), HttpStatus.CREATED);
    }

    @GetMapping
    public ResponseEntity<List<PersonDto>> list() {
        List<PersonDto> personDto = studentService.list().stream().map(PersonDto::from).collect(toList());

        return new ResponseEntity<>(personDto, HttpStatus.OK);
    }

    @GetMapping("{id}")
    public ResponseEntity<PersonDto> get(@PathVariable Long id) {
        Student student = studentService.get(id);

        return new ResponseEntity<>(PersonDto.from(student), HttpStatus.OK);
    }

    @PutMapping("{id}")
    public ResponseEntity<PersonDto> update(@PathVariable Long id,
                                            @RequestBody PersonDto personDto) {
        Student student = studentService.update(id, personDto.toPerson(Student.class));

        return new ResponseEntity<>(PersonDto.from(student), HttpStatus.OK);
    }

    @DeleteMapping("{id}")
    public ResponseEntity<PersonDto> delete(@PathVariable Long id) {
        Student student = studentService.delete(id);

        return new ResponseEntity<>(PersonDto.from(student), HttpStatus.OK);
    }

}
