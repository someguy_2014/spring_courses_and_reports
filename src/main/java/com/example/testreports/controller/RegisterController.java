package com.example.testreports.controller;

import com.example.testreports.model.dto.CourseDto;
import com.example.testreports.model.dto.GroupDto;
import com.example.testreports.model.dto.PersonDto;
import com.example.testreports.model.entity.Course;
import com.example.testreports.model.entity.Group;
import com.example.testreports.model.entity.Student;
import com.example.testreports.model.entity.Teacher;
import com.example.testreports.service.RegisterService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

import static java.util.stream.Collectors.toSet;

@RestController
@RequestMapping("/course/{courseId}")
public class RegisterController {

    private final RegisterService registerService;

    public RegisterController(RegisterService registerService) {
        this.registerService = registerService;
    }

    @PutMapping("/group/{id}/students")
    public ResponseEntity<GroupDto> setStudentsToGroup(@PathVariable Long courseId,
                                                       @PathVariable Long id,
                                                       @RequestBody List<PersonDto> personDto) {

        Set<Student> students = personDto.stream().map(student -> student.toPerson(Student.class)).collect(toSet());
        Group group = registerService.setStudentsToGroup(courseId, id, students);

        return new ResponseEntity<>(GroupDto.from(group), HttpStatus.OK);
    }

    @PutMapping("/teacher")
    public ResponseEntity<CourseDto> setTeacherToCourse(@PathVariable Long courseId,
                                                        @RequestBody PersonDto personDto) {

        Teacher teacher = personDto.toPerson(Teacher.class);
        Course course = registerService.setTeacherToCourse(courseId, teacher);

        return new ResponseEntity<>(CourseDto.from(course), HttpStatus.OK);
    }
}
