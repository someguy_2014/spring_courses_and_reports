package com.example.testreports.controller;

import com.example.testreports.model.dto.PersonDto;
import com.example.testreports.model.entity.Teacher;
import com.example.testreports.service.TeacherService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static java.util.stream.Collectors.toList;

@RestController
@RequestMapping("/teacher")
public class TeacherController {

    private final TeacherService teacherService;

    public TeacherController(TeacherService teacherService) {
        this.teacherService = teacherService;
    }

    @PostMapping
    public ResponseEntity<PersonDto> add(@RequestBody PersonDto personDto) {
        Teacher teacher = teacherService.add(personDto.toPerson(Teacher.class));

        return new ResponseEntity<>(PersonDto.from(teacher), HttpStatus.CREATED);
    }

    @GetMapping
    public ResponseEntity<List<PersonDto>> list() {
        List<PersonDto> personDto = teacherService.list().stream().map(PersonDto::from).collect(toList());

        return new ResponseEntity<>(personDto, HttpStatus.OK);
    }

    @GetMapping("{id}")
    public ResponseEntity<PersonDto> get(@PathVariable Long id) {
        Teacher teacher = teacherService.get(id);

        return new ResponseEntity<>(PersonDto.from(teacher), HttpStatus.OK);
    }

    @PutMapping("{id}")
    public ResponseEntity<PersonDto> update(@PathVariable Long id,
                                            @RequestBody PersonDto personDto) {
        Teacher teacher = teacherService.update(id, personDto.toPerson(Teacher.class));

        return new ResponseEntity<>(PersonDto.from(teacher), HttpStatus.OK);
    }

    @DeleteMapping("{id}")
    public ResponseEntity<PersonDto> delete(@PathVariable Long id) {
        Teacher teacher = teacherService.delete(id);

        return new ResponseEntity<>(PersonDto.from(teacher), HttpStatus.OK);
    }

}
