package com.example.testreports.controller;

import com.example.testreports.model.dto.CourseDto;
import com.example.testreports.model.entity.Course;
import com.example.testreports.service.CourseService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static java.util.stream.Collectors.toList;

@RestController
@RequestMapping("/course")
public class CourseController {

    private final CourseService courseService;

    public CourseController(CourseService courseService) {
        this.courseService = courseService;
    }

    @PostMapping
    public ResponseEntity<CourseDto> add(@RequestBody CourseDto courseDto) {
        Course course = courseService.add(courseDto.toCourse());

        return new ResponseEntity<>(CourseDto.from(course), HttpStatus.CREATED);
    }

    @GetMapping
    public ResponseEntity<List<CourseDto>> list() {
        List<CourseDto> courseDto = courseService.list().stream().map(CourseDto::from).collect(toList());

        return new ResponseEntity<>(courseDto, HttpStatus.OK);
    }

    @GetMapping("{id}")
    public ResponseEntity<CourseDto> get(@PathVariable Long id) {
        Course course = courseService.get(id);

        return new ResponseEntity<>(CourseDto.from(course), HttpStatus.OK);
    }

    @PutMapping("{id}")
    public ResponseEntity<CourseDto> update(@PathVariable Long id,
                                            @RequestBody CourseDto courseDto) {
        Course course = courseService.update(id, courseDto.toCourse());

        return new ResponseEntity<>(CourseDto.from(course), HttpStatus.OK);
    }

    @DeleteMapping("{id}")
    public ResponseEntity<CourseDto> delete(@PathVariable Long id) {
        Course course = courseService.delete(id);

        return new ResponseEntity<>(CourseDto.from(course), HttpStatus.OK);
    }

}
