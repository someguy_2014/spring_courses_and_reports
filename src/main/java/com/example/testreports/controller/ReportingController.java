package com.example.testreports.controller;

import com.example.testreports.model.dto.PersonDto;
import com.example.testreports.model.enums.CourseType;
import com.example.testreports.service.ReportingService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

import static java.util.stream.Collectors.toList;

@RestController
@RequestMapping("/report")
public class ReportingController {

    private final ReportingService reportingService;

    public ReportingController(ReportingService reportingService) {
        this.reportingService = reportingService;
    }

    @GetMapping("/studentsCount")
    public ResponseEntity<Long> studentsCount(){
        return new ResponseEntity<>(reportingService.getStudentsCount(), HttpStatus.OK);
    }

    @GetMapping("/teachersCount")
    public ResponseEntity<Long> teachersCount(){
        return new ResponseEntity<>(reportingService.getTeachersCount(), HttpStatus.OK);
    }

    @GetMapping("/coursesCountByType")
    public ResponseEntity<Long> coursesCountByType(CourseType courseType){
        return new ResponseEntity<>(reportingService.getCoursesCountByType(courseType), HttpStatus.OK);
    }

    @GetMapping("/studentsByCourse")
    public ResponseEntity<List<PersonDto>> studentsByCourse(@RequestParam Long courseId){
        List<PersonDto> result = reportingService.getStudentsByCourse(courseId).stream().map(PersonDto::from).collect(toList());

        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @GetMapping("/studentsByGroup")
    public ResponseEntity<List<PersonDto>> studentsByGroup(@RequestParam Long groupId){
        List<PersonDto> result = reportingService.getStudentsByGroup(groupId)
                .stream().map(PersonDto::from).collect(toList());

        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @GetMapping("/personsByCourseAndGroup")
    public ResponseEntity<List<PersonDto>> personsByCourseAndGroup(@RequestParam Long courseId, @RequestParam Long groupId){
        List<PersonDto> result = new ArrayList<>();
        reportingService.getStudentsByCourseAndGroup(courseId, groupId).forEach(student -> {
            result.add(PersonDto.from(student));
        });
        reportingService.getTeachersByCourseAndGroup(courseId, groupId).forEach(teacher -> {
            result.add(PersonDto.from(teacher));
        });

        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @GetMapping("/studentsByCourseOlderThan")
    public ResponseEntity<List<PersonDto>> studentsByCourseOlderThan(@RequestParam Long courseId, @RequestParam int age){
        List<PersonDto> result = reportingService.getStudentsByCourseOlderThan(courseId, age)
                .stream().map(PersonDto::from).collect(toList());

        return new ResponseEntity<>(result, HttpStatus.OK);
    }
}
