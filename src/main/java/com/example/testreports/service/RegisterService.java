package com.example.testreports.service;

import com.example.testreports.model.entity.Course;
import com.example.testreports.model.entity.Group;
import com.example.testreports.model.entity.Student;
import com.example.testreports.model.entity.Teacher;

import java.util.Set;

import static java.util.stream.Collectors.toSet;

public class RegisterService {

    private final StudentService studentService;
    private final TeacherService teacherService;
    private final CourseService courseService;
    private final GroupService groupService;

    public RegisterService(StudentService studentService, TeacherService teacherService,
                           CourseService courseService, GroupService groupService) {
        this.studentService = studentService;
        this.teacherService = teacherService;
        this.courseService = courseService;
        this.groupService = groupService;
    }

    public Group setStudentsToGroup(Long courseId, Long id, Set<Student> students) {
        return groupService.update(courseId, id,
                students.stream().map(student -> studentService.get(student.getId())).collect(toSet()));
    }

    public Course setTeacherToCourse(Long courseId, Teacher teacher) {
        return courseService.update(courseId, teacherService.get(teacher.getId()));
    }
}
