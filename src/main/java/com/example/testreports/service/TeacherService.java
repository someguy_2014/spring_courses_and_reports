package com.example.testreports.service;

import com.example.testreports.model.entity.Teacher;
import com.example.testreports.repository.TeacherRepository;

import java.text.MessageFormat;
import java.util.List;

import org.springframework.data.rest.webmvc.ResourceNotFoundException;

public class TeacherService {

    public static final String RESOURCE_NOT_FOUND_ERROR_MESSAGE_TEMPLATE = "Could not find teacher with id: {0}";

    private final TeacherRepository teacherRepository;

    public TeacherService(TeacherRepository teacherRepository) {
        this.teacherRepository = teacherRepository;
    }

    public List<Teacher> list() {
        return teacherRepository.findAll();
    }

    public Teacher add(Teacher teacher) {
        return teacherRepository.save(teacher);
    }

    public Teacher get(Long id) {
        return teacherRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException(
                        MessageFormat.format(RESOURCE_NOT_FOUND_ERROR_MESSAGE_TEMPLATE, id)
                )
        );
    }

    public Teacher update(Long id, Teacher teacher) {
        Teacher teacherToEdit = get(id);

        teacherToEdit.setPerson(teacher.getPerson());

        return teacherRepository.save(teacherToEdit);
    }

    public Teacher delete(Long id) {
        Teacher teacher = get(id);

        teacherRepository.delete(teacher);

        return teacher;

    }
}
