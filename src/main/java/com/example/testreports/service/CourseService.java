package com.example.testreports.service;

import com.example.testreports.model.entity.Course;
import com.example.testreports.model.entity.Teacher;
import com.example.testreports.repository.CourseRepository;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;

import java.text.MessageFormat;
import java.util.List;

public class CourseService {

    public static final String RESOURCE_NOT_FOUND_ERROR_MESSAGE_TEMPLATE = "Could not find course with id: {0}";

    private final CourseRepository courseRepository;

    public CourseService(CourseRepository courseRepository) {
        this.courseRepository = courseRepository;
    }

    public Course add(Course course) {
        return courseRepository.save(course);
    }

    public List<Course> list() {
        return courseRepository.findAll();
    }

    public Course get(Long id) {
        return courseRepository.findById(id).orElseThrow(() ->
                new ResourceNotFoundException(MessageFormat.format(RESOURCE_NOT_FOUND_ERROR_MESSAGE_TEMPLATE, id)
                )
        );
    }

    public Course update(Long id, Course course) {
        Course courseToEdit = get(id);

        courseToEdit.setName(course.getName());
        courseToEdit.setType(course.getType());

        return courseRepository.save(courseToEdit);
    }

    public Course update(Long id, Teacher teacher) {
        Course courseToEdit = get(id);

        courseToEdit.setTeacher(teacher);

        return courseRepository.save(courseToEdit);
    }

    public Course delete(Long id) {
        Course course = get(id);

        courseRepository.delete(course);

        return course;
    }

}
