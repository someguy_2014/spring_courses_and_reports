package com.example.testreports.service;

import com.example.testreports.model.entity.Course;
import com.example.testreports.model.entity.Group;
import com.example.testreports.model.entity.Student;
import com.example.testreports.model.entity.Teacher;
import com.example.testreports.repository.GroupRepository;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;

import java.text.MessageFormat;
import java.util.List;
import java.util.Set;

public class GroupService {

    public static final String RESOURCE_NOT_FOUND_ERROR_MESSAGE_TEMPLATE =
            "Could not find group with id: {0} for course id: {1}";

    private final GroupRepository groupRepository;

    public GroupService(GroupRepository groupRepository) {
        this.groupRepository = groupRepository;
    }

    public List<Group> list(Long courseId) {
        return groupRepository.findByCourseId(courseId);
    }

    public Group get(Long courseId, Long id) {
        return groupRepository.findByIdAndCourseId(id, courseId).orElseThrow(() ->
                new ResourceNotFoundException(
                        MessageFormat.format(RESOURCE_NOT_FOUND_ERROR_MESSAGE_TEMPLATE, id, courseId)
                )
        );
    }

    public Group add(Course course, Group group) {
        group.setCourse(course);

        return groupRepository.save(group);
    }

    public Group update(Long courseId, Long id, Group group) {
        Group groupToEdit = get(courseId, id);

        groupToEdit.setName(group.getName());

        return groupRepository.save(groupToEdit);
    }

    public Group update(Long courseId, Long id, Set<Student> students) {
        Group groupToEdit = get(courseId, id);

        groupToEdit.setStudents(students);

        return groupRepository.save(groupToEdit);
    }

    public Group delete(Long courseId, Long id) {
        Group group = get(courseId, id);

        groupRepository.delete(group);

        return group;
    }

}
