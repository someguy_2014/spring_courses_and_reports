package com.example.testreports.service;

import com.example.testreports.model.entity.Student;
import com.example.testreports.repository.StudentRepository;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;

import java.text.MessageFormat;
import java.util.List;

public class StudentService {

    public static final String RESOURCE_NOT_FOUND_ERROR_MESSAGE_TEMPLATE = "Could not find student with id: {0}";

    private final StudentRepository studentRepository;

    public StudentService(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    public List<Student> list() {
        return studentRepository.findAll();
    }

    public Student add(Student student) {
        return studentRepository.save(student);
    }

    public Student get(Long id) {
        return studentRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException(
                        MessageFormat.format(RESOURCE_NOT_FOUND_ERROR_MESSAGE_TEMPLATE, id)
                )
        );
    }

    public Student update(Long id, Student student) {
        Student studentToEdit = get(id);

        studentToEdit.setPerson(student.getPerson());

        return studentRepository.save(studentToEdit);
    }

    public Student delete(Long id) {
        Student student = get(id);

        studentRepository.delete(student);

        return student;
    }
}
