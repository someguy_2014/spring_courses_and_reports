package com.example.testreports.service;

import com.example.testreports.model.entity.Course;
import com.example.testreports.model.entity.IPerson;
import com.example.testreports.model.entity.Student;
import com.example.testreports.model.entity.Teacher;
import com.example.testreports.model.enums.CourseType;
import com.example.testreports.repository.CourseRepository;
import com.example.testreports.repository.GroupRepository;
import com.example.testreports.repository.StudentRepository;
import com.example.testreports.repository.TeacherRepository;
import org.springframework.data.domain.Example;

import java.util.List;

public class ReportingService {

    private final TeacherRepository teacherRepository;
    private final StudentRepository studentRepository;
    private final CourseRepository courseRepository;

    public ReportingService(StudentRepository studentRepository, TeacherRepository teacherRepository,
                            CourseRepository courseRepository) {
        this.teacherRepository = teacherRepository;
        this.studentRepository = studentRepository;
        this.courseRepository = courseRepository;
    }

    public long getStudentsCount(){
        return studentRepository.count();
    }

    public long getTeachersCount(){
        return teacherRepository.count();
    }

    public long getCoursesCountByType(CourseType courseType){
        Course course = new Course();

        course.setType(courseType);

        return courseRepository.count(Example.of(course));
    }

    public List<Student> getStudentsByCourse(Long courseId){
        return studentRepository.getStudentsByCourse(courseId);
    }

    public List<Student> getStudentsByGroup(Long groupId){
        return studentRepository.getStudentsByGroup(groupId);
    }

    public List<Student> getStudentsByCourseAndGroup(Long courseId, Long groupId){
        return studentRepository.getStudentsByCourseAndGroup(courseId, groupId);
    }

    public List<Teacher> getTeachersByCourseAndGroup(Long courseId, Long groupId){
        return teacherRepository.getTeachersByCourseAndGroup(courseId, groupId);
    }

    public List<Student> getStudentsByCourseOlderThan(Long courseId, int age){
        return studentRepository.getStudentsByCourseOlderThan(courseId, age);
    }
}
